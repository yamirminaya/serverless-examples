const AWS = require("aws-sdk");
const sns = new AWS.SNS();

const sendSNS = async (event, context) => {
  const params = {
    Message: "Este es un mensaje de prueba enviado desde una función Lambda",
    TopicArn: "arn:aws:sns:us-east-1:071173831616:TestA2-IFX",
  };
  try {
    const data = await sns.publish(params).promise();
    console.log("Mensaje publicado con éxito: ", data);
    return context.succeed(data);
  } catch (err) {
    console.error("No se pudo publicar el mensaje: ", err);
    return context.fail(err);
  }
};

module.exports = { sendSNS };
