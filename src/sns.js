// const express = require("express");
// const bodyParser = require("body-parser");

// const app = express();
// app.use(bodyParser.json());

const middy = require("@middy/core");
const jsonBodyParser = require("@middy/http-json-body-parser");

const sns = (event, context, callback) => {
  //   app.post("/sns", (req, res) => {
  const body = JSON.parse(event.body);
  if (body.Type === "SubscriptionConfirmation") {
    // Procesar mensaje de confirmación de suscripción
    console.log(`SubscriptionConfirmation received: ${body.SubscribeURL}`);
    // Confirmar la suscripción a SNS
  } else if (body.Type === "Notification") {
    // Procesar mensaje de notificación
    console.log(`Notification received: ${body.Message}`);
    // Realizar la acción correspondiente con la información del mensaje
  }

  return {
    statusCode: 200,
    body: "",
  };

  //   });
};

module.exports = { sns: middy(sns).use(jsonBodyParser()) };
