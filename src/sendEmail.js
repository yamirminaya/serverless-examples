const { v4 } = require("uuid");
const AWS = require("aws-sdk");

const middy = require("@middy/core");
const jsonBodyParser = require("@middy/http-json-body-parser");

var SibApiV3Sdk = require("sib-api-v3-sdk");
var defaultClient = SibApiV3Sdk.ApiClient.instance;

// Configure API key authorization: api-key
var apiKey = defaultClient.authentications["api-key"];
apiKey.apiKey =
  "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";
var apiInstance = new SibApiV3Sdk.TransactionalEmailsApi();
var sendSmtpEmail = new SibApiV3Sdk.SendSmtpEmail(); // SendSmtpEmail | Values to send a transactional email

const sendEmail = async (event) => {
  let {
    subject = "",
    to = [],
    cc = [],
    bcc = [],
    htmlContent = "",
    params = {},
    attachment = [],
  } = event.body;

  try {
    sendSmtpEmail = {
      subject,
      to,
      cc,
      bcc,
      htmlContent,
      params,
      attachment,
    };

    sendSmtpEmail.sender = { email: "api@sendinblue.com", name: "Sendinblue" };
    sendSmtpEmail.replyTo = { email: "api@sendinblue.com", name: "Sendinblue" };

    let data = await apiInstance.sendTransacEmail(sendSmtpEmail);

    // dynamoDB
    const dynamodb = new AWS.DynamoDB.DocumentClient();

    let newMessage = {
      id: v4(),
      messageId: data.messageId,
    };

    await dynamodb
      .put({
        TableName: "LogMessages",
        Item: newMessage,
      })
      .promise();

    return {
      statusCode: 200,
      body: JSON.stringify({ data }),
    };
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({ error }),
    };
  }
};

module.exports = { sendEmail: middy(sendEmail).use(jsonBodyParser()) };
