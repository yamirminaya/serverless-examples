const AWS = require("aws-sdk");

const uploadFile = async (event) => {
  // Inicializa una instancia de S3
  const s3 = new AWS.S3();

  // Obtiene el archivo y la información asociada desde la solicitud
  const file = event.body;
  const filename = event.headers["x-file-name"];

  // Configura los parámetros de la operación de carga en S3
  const params = {
    Bucket: "nombre-del-bucket",
    Key: filename,
    Body: file,
  };

  // Realiza la operación de carga en S3
  try {
    const upload = await s3.upload(params).promise();
    return {
      statusCode: 200,
      body: JSON.stringify({
        message: "File uploaded successfully",
        file: upload,
      }),
    };
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({
        message: "File upload failed",
        error: error,
      }),
    };
  }
};

module.exports = { uploadFile };
